# Exodeon

Hello there ! 👋  
My name is Robin Langlois and I'm a software engineer.

I'm passionated in coding 💻 (quite obviously) and video games 🎮.

## Some personal projects
- [A Discord bot that I'm using on a server with my friends](https://gitlab.com/Exodeon/polka-bot)
- Some fun genetic algorithms projects : [a sentence finder](https://gitlab.com/Exodeon/genetic-sentence-finder), [a way to find an optimal Pokémon team](https://gitlab.com/Exodeon/genetic-pokemon-team), [an AI to play the Chrome dinosaur game](https://gitlab.com/Exodeon/genetic-dinosaurs)
- [Powertower](https://gitlab.com/pygame-projects/powertower/-/blob/master/README.md), a small game made with Pygame

## Main skills
<img src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white" alt="Python"/>
<img src="https://img.shields.io/badge/C%2B%2B-00599C?style=for-the-badge&logo=c%2B%2B&logoColor=white" alt="C++"/>

## How to reach me

<a href="https://www.linkedin.com/in/robin-langlois/">
    <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn"/>
  </a>
<a href="https://www.codingame.com/profile/678450bbd771739ca21456ab6b60995e1893002">
    <img src="https://img.shields.io/static/v1?style=for-the-badge&message=CodinGame&color=222222&logo=CodinGame&logoColor=F2BB13&label=" alt="CodinGame"/>
  </a>
<a href="https://e-xodeon.itch.io/">
    <img src="https://img.shields.io/badge/Itch.io-FA5C5C?style=for-the-badge&logo=itchdotio&logoColor=white" alt="itch.io"/>
  </a>

